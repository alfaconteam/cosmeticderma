﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SLnet.Base.Customization.Interfaces;
using SLnet.Sand.Base.Customization.Interfaces;
using SLnet.Base.Interfaces;
using SLnet.Sand.WinForms.Forms;
using SLnet.Base.Customization;
using SLnet.Sand.Base.Interfaces;
using Crm.WUI.CRM.Activities;
using DevExpress.XtraLayout;
using Crm.Core.WinControls.AdvControls;
using SLnet.Base.DataObjects;
using SLnet.Sand.Base.Forms;
using Crm.Data.DataObjects;
using Crm.Core.Base;

namespace aspCosmeticDerma.WUI.CRM.Activities
{

    [slRegisterCustomDomainAppExtension()]
    public class aspasDomainAppExtension : IslCustomDomainAppExtension, IslsCustomDomainAppFormExtension
    {
        IslAppContext _appContext; 

        public aspasDomainAppExtension()
        {
        }


        public void Create(IslAppContext appContext, IslsForm form)
        {

        }

        public void ExecuteLink(IslAppContext appContext, IslsForm form, string command)
        {

        }

        public Dictionary<string, string> GetLinks(IslAppContext appContext, IslsForm form)
        {

            Dictionary<string, string> dc = null;
            return dc;
        }

        public void Init(IslAppContext appContext, IslsForm form)
        {
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.Activities.Activities.cmSalesOpportunityF":
                case "Crm.WUI.CRM.Activities.cmAppointmentF":
                case "Crm.WUI.CRM.Activities.cmTaskF":
                case "Crm.WUI.CRM.Activities.cmCallF":
                case "Crm.WUI.CRM.Activities.cmRequestF":

                    _appContext = appContext;
                    cmActivitiesCollection cCol = (form as cmActivitiesF).Items;
                    cCol.PreviewPropertyChanged += new EventHandler<SLnet.Base.Utils.slEventArgs<slPreviewPropChangedArgs>>(cCol_PreviewPropertyChanged);
                    break;
                default:
                    break;
            }
        }

        private void cCol_PreviewPropertyChanged(object sender, SLnet.Base.Utils.slEventArgs<slPreviewPropChangedArgs> e)
        {
            if (sender is cmActivitiesDataObject)
            {
                cmActivitiesDataObject acObj = sender as cmActivitiesDataObject;
                switch (e.EventData.PropertyName)
                {
                    case "AccountID":

                        IslObjectActivator oa = _appContext.ServiceLocator.GetService<IslObjectActivator>();

                        using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.Contacts)))
                        {
                            cmContactsCollection dc = (cmContactsCollection)obj.ExecuteOperation("GetByID", acObj.AccountID);
                            cmContactsDataObject cObj = dc[0];
                            MessageBox.Show(cObj.AlertMessage.ToString());
                        }

                        break;
                    default:
                        break;
                }
            }
        
        }


        public void SyncScreen(IslAppContext appContext, IslsForm form)
        {
            

        }



    }

}
