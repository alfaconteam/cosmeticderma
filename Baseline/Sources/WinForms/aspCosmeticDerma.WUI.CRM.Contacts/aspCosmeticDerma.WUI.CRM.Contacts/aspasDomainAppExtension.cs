﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SLnet.Base.Customization.Interfaces;
using SLnet.Sand.Base.Customization.Interfaces;
using SLnet.Base.Interfaces;
using SLnet.Sand.WinForms.Forms;
using SLnet.Base.Customization;
using SLnet.Sand.Base.Interfaces;
using Crm.WUI.CRM.Contacts;
using DevExpress.XtraLayout;
using Crm.Core.WinControls.AdvControls;
using SLnet.Base.DataObjects;
using SLnet.Sand.Base.Forms;
using Crm.Data.DataObjects;

namespace aspCosmeticDerma.WUI.CRM.Contacts
{

    [slRegisterCustomDomainAppExtension()]
    public class aspasDomainAppExtension : IslCustomDomainAppExtension, IslsCustomDomainAppFormExtension
    {

        public aspasDomainAppExtension()
        {
        }


        public void Create(IslAppContext appContext, IslsForm form)
        {

        }

        public void ExecuteLink(IslAppContext appContext, IslsForm form, string command)
        {

        }

        public Dictionary<string, string> GetLinks(IslAppContext appContext, IslsForm form)
        {

            Dictionary<string, string> dc = null;
            return dc;
        }

        public void Init(IslAppContext appContext, IslsForm form)
        {

        }

        public void SyncScreen(IslAppContext appContext, IslsForm form)
        {
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.Contacts.cmContactsF":
                    //_appContext = appContext;
                    cmContactsCollection cCol = (form as cmContactsF).Items;
                    cmContactsDataObject cObj = cCol[0] as cmContactsDataObject;
                    if (!String.IsNullOrWhiteSpace(cObj.AlertMessage))
                    {
                        MessageBox.Show(cObj.AlertMessage);
                    }
                    break;
                case "Crm.WUI.CRM.Contacts.cmContactPersonsF":
                    //_appContext = appContext;
                    cmContactsCollection cColPer = (form as cmContactsF).Items;
                    cmContactsDataObject cObjPer = cColPer[0] as cmContactsDataObject;
                    if (cObjPer.IsPerson == 1 && cObjPer.Sex == cmContactsDataObject.SexEnum.Unidentified)
                    {
                        MessageBox.Show("Παρακαλώ πολύ δηλώστε φύλο !!! ");
                    }
                    if (!String.IsNullOrWhiteSpace(cObjPer.AlertMessage))
                    {
                        MessageBox.Show(cObjPer.AlertMessage);
                    }
                    break;
                default:
                    break;
            }

        }



    }

}
