﻿using System;
using SLnet.Base.Customization.Interfaces;
using SLnet.Base.Customization;
using SLnet.Base.Interfaces;
using Crm.Data.DataObjects;
using Crm.Core.Base.Trace;


namespace aspCosmeticDerma.ENT.CRM.Contact
{
    [slRegisterCustomDomainAppExtension()]
    public class aspasDomainAppExtension : IslCustomDomainAppExtension, IslCustomDomainAppObjectExtension
    {


        public void AfterExecuteOperation(ref object res, SLnet.Base.Interfaces.IslAppContext appContext,
            SLnet.Base.Interfaces.IslOperations container, SLnet.Base.slBaseOperation operation, params object[] args)
        {
            try
            {
                switch (operation.ToString())
                {
                    case "Crm.ENT.CRM.Contacts.cmContacts+opServer_PreSaveProcessing":
                        cmContactsCollection col = args[0] as cmContactsCollection;
                        cmContactsDataObject obj = col[0];
                        if (obj.IsPerson == 1 && obj.Sex == cmContactsDataObject.SexEnum.Unidentified)
                        {
                            throw new Exception("Παρακαλώ πολύ δηλώστε φύλο !!! ");
                        }
                        break;

                }
            }
            catch (Exception)
            {
                    
                throw;
            }
        }


        public void BeforeExecuteOperation(SLnet.Base.Interfaces.IslAppContext appContext, SLnet.Base.Interfaces.IslOperations container, SLnet.Base.slBaseOperation operation, params object[] args)
        {
        }


    }
}
