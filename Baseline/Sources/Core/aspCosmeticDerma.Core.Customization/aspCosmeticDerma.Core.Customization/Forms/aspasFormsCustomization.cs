﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SLnet.Base.Customization.Interfaces;
using SLnet.Sand.Base.Customization.Interfaces;
using SLnet.Base.Customization;
using SLnet.Base.Interfaces;
using SLnet.Sand.Base.Customization.UI;
using aspCosmeticDerma.Core.Base;

namespace aspCosmeticDerma.Core.Customization.Forms
{

    [slRegisterCustomDomainAppExtension()]
    public class aspasFormsCustomization : IslCustomDomainAppExtension, IslsCustomDomainAppFormRegExtension
    {

        private slsFormsCustomizationManager fmManager;

        private void RegisterForm(string alias, string description)
        {
            fmManager.RegisterForm(alias, description);
        }

        public void RegisterForms(IslAppContext appContext, slsFormsCustomizationManager formManager)
        {
            fmManager = formManager;

        }

    }

}
