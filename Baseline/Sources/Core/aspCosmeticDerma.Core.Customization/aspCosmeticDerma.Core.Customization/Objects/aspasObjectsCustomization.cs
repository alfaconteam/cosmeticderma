﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SLnet.Base.Customization;
using SLnet.Base.Customization.Interfaces;
using SLnet.Sand.Base.Customization.Interfaces;
using SLnet.Base.Interfaces;
using SLnet.Sand.Base.Customization.Objects;
using aspCosmeticDerma.Core.Base;

namespace aspCosmeticDerma.Core.Customization.Objects
{

    [slRegisterCustomDomainAppExtension()]
    public class aspasObjectsCustomization : IslCustomDomainAppExtension, IslsCustomDomainAppObjectRegExtension
    {

        private slsObjectsCustomizationManager objManager;

        private void RegisterObject(string alias, string description)
        {
            objManager.RegisterObject(alias, description);
        }

        public void RegisterObjects(IslAppContext appContext, slsObjectsCustomizationManager objectManager)
        {
            objManager = objectManager;

        }

    }

}
